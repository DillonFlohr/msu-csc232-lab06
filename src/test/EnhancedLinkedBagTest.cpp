/**
 * CSC232 Data Structures with C++
 * Missouri State University, Spring 2017.
 *
 * @file   EnhancedLinkedBagTest.cpp
 * @author Jim Daehn <jdaehn@missouristate.edu>
 * @brief  Implementation of Enhanced Array Bag Unit Test.
 * @see    http://sourceforge.net/projects/cppunit/files
 *
 * @copyright Jim Daehn, 2017. All rights reserved.
 */

#include <iostream>
#include "EnhancedLinkedBagTest.h"

CPPUNIT_TEST_SUITE_REGISTRATION(EnhancedLinkedBagTest);
const std::vector<std::string> EnhancedLinkedBagTest::lhdata = {"a", "b", "c"};
const std::vector<std::string> EnhancedLinkedBagTest::rhdata = {"b", "b", "d", "e"};
const std::vector<std::string> EnhancedLinkedBagTest::expectedUnion{"a", "b", "b", "b", "c", "d", "e"};
const std::vector<std::string> EnhancedLinkedBagTest::expectedIntersection{"b"};
const std::vector<std::string> EnhancedLinkedBagTest::expectedDifference{"a", "c"};

const int EnhancedLinkedBagTest::EXPECTED_UNION_SIZE = EnhancedLinkedBagTest::expectedUnion.size();
const int EnhancedLinkedBagTest::EXPECTED_INTERSECTION_SIZE = EnhancedLinkedBagTest::expectedIntersection.size();
const int EnhancedLinkedBagTest::EXPECTED_DIFFERENCE_SIZE = EnhancedLinkedBagTest::expectedDifference.size();

/* Function prototype for utility function */
template<class T>
void assertFrequencies(EnhancedLinkedBag<T> expected, EnhancedLinkedBag<T> actual);

EnhancedLinkedBagTest::EnhancedLinkedBagTest() {
    // no-op
}

EnhancedLinkedBagTest::~EnhancedLinkedBagTest() {
    // no-op
}

/**
 * Operation that executes just before each unit test.
 */
void EnhancedLinkedBagTest::setUp() {
    for (auto element : lhdata) {
        lhbag.add(element);
    }

    for (auto element : rhdata) {
        rhbag.add(element);
    }
}

/**
 * Operation that executes just after each unit test.
 */
void EnhancedLinkedBagTest::tearDown() {
    lhbag.clear();
    rhbag.clear();
}

/**
 * Assert that the results of the union operattion contain the correct frequencies 
 * of each expected item.
 */
void EnhancedLinkedBagTest::testUnionResults() {
    EnhancedLinkedBag<std::string> expectedUnionBag;
    expectedUnionBag.add("a");
    expectedUnionBag.add("b");
    expectedUnionBag.add("b");
    expectedUnionBag.add("b");
    expectedUnionBag.add("c");
    expectedUnionBag.add("d");
    expectedUnionBag.add("e");
    EnhancedLinkedBag<std::string> actualUnionBag{lhbag.unionWithBag(rhbag)};
    assertFrequencies(expectedUnionBag, actualUnionBag);
}

/**
 * Assert that the expected size is as expected to ensure no extraneous information
 * has been added or subtracted from the union.
 */
void EnhancedLinkedBagTest::testUnionResultSize() {
    EnhancedLinkedBag<std::string> unionBag{lhbag.unionWithBag(rhbag)};
    CPPUNIT_ASSERT_EQUAL_MESSAGE("The size of the actual bag disagrees with the expected size.",
        EnhancedLinkedBagTest::EXPECTED_UNION_SIZE, unionBag.getCurrentSize());
}

/**
 * Assert that the results of the intersection operattion contain the correct frequencies 
 * of each expected item.
 */
void EnhancedLinkedBagTest::testIntersectionResults() {
    EnhancedLinkedBag<std::string> expectedIntersectionBag;
    expectedIntersectionBag.add("b");
    EnhancedLinkedBag<std::string> actualIntersectionBag{lhbag.intersectionWithBag(rhbag)};
    assertFrequencies(expectedIntersectionBag, actualIntersectionBag);
}

/**
 * Assert that the expected size is as expected to ensure no extraneous information
 * has been added or subtracted from the intersection.
 */
void EnhancedLinkedBagTest::testIntersectionResultSize() {
    EnhancedLinkedBag<std::string> intersectionBag{lhbag.intersectionWithBag(rhbag)}; 
    CPPUNIT_ASSERT_EQUAL_MESSAGE("The size of the actual bag disagrees with the expected size.",
        EnhancedLinkedBagTest::EXPECTED_INTERSECTION_SIZE, intersectionBag.getCurrentSize());
}

/**
 * Assert that the results of the difference operattion contain the correct frequencies 
 * of each expected item.
 */
void EnhancedLinkedBagTest::testDifferenceResults() {
    EnhancedLinkedBag<std::string> expectedDifferenceBag;
    expectedDifferenceBag.add("a");
    expectedDifferenceBag.add("c");
    EnhancedLinkedBag<std::string> actualDifferenceBag{lhbag.differenceWithBag(rhbag)};
    assertFrequencies(expectedDifferenceBag, actualDifferenceBag);
}

/**
 * Assert that the expected size is as expected to ensure no extraneous information
 * has been added or subtracted from the difference.
 */
void EnhancedLinkedBagTest::testDifferenceResultSize() {
    EnhancedLinkedBag<std::string> differenceBag{lhbag.differenceWithBag(rhbag)};
    CPPUNIT_ASSERT_EQUAL_MESSAGE("The size of the actual bag disagrees with the expected size.",
        EnhancedLinkedBagTest::EXPECTED_DIFFERENCE_SIZE, differenceBag.getCurrentSize());
}

/**
 * Utility function that verifies the expected frequencies of each element.
 *
 * @param expected the expected bag results
 * @param actual   the actual bag results
 */
template<class T>
void assertFrequencies(EnhancedLinkedBag<T> expected, EnhancedLinkedBag<T> actual) {
    CPPUNIT_ASSERT_EQUAL_MESSAGE("frequency of \"a\" assertion failed.", expected.getFrequencyOf("a"), actual.getFrequencyOf("a"));
    CPPUNIT_ASSERT_EQUAL_MESSAGE("frequency of \"b\" assertion failed.", expected.getFrequencyOf("b"), actual.getFrequencyOf("b"));
    CPPUNIT_ASSERT_EQUAL_MESSAGE("frequency of \"c\" assertion failed.", expected.getFrequencyOf("c"), actual.getFrequencyOf("c"));
    CPPUNIT_ASSERT_EQUAL_MESSAGE("frequency of \"d\" assertion failed.", expected.getFrequencyOf("d"), actual.getFrequencyOf("d"));
    CPPUNIT_ASSERT_EQUAL_MESSAGE("frequency of \"e\" assertion failed.", expected.getFrequencyOf("e"), actual.getFrequencyOf("e"));
}
